import java.util.Scanner;

public class Zaehlen {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Natürliche Zahl: ");
		int zahl = scan.nextInt();
		
		for(int zaehler = 1; zaehler <=zahl ; zaehler++) {
			System.out.println(zaehler);
		}
	}
}
